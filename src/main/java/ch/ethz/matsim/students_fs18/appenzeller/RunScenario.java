package ch.ethz.matsim.students_fs18.appenzeller;

import org.matsim.api.core.v01.Scenario;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.controler.Controler;
import org.matsim.core.scenario.ScenarioUtils;

import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.baseline_scenario.BaselineModule;
import ch.ethz.matsim.baseline_scenario.analysis.simulation.ModeShareListenerModule;
import ch.ethz.matsim.baseline_scenario.transit.BaselineTransitModule;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRoute;
import ch.ethz.matsim.baseline_scenario.transit.routing.DefaultEnrichedTransitRouteFactory;
import ch.ethz.matsim.baseline_scenario.zurich.ZurichModule;
import ch.ethz.matsim.projects.astra.analysis.AnalysisModule;
import ch.ethz.matsim.projects.astra.av.ASTRAAVModule;
import ch.ethz.matsim.projects.astra.av.ASTRAQSimModule;
import ch.ethz.matsim.projects.astra.av.pricing.AVPriceCalculationConfigGroup;
import ch.ethz.matsim.projects.astra.av.pricing.AVPriceCalculationModule;
import ch.ethz.matsim.projects.astra.av.waiting_time.AVWaitingTimeCalculatorConfigGroup;
import ch.ethz.matsim.projects.astra.av.waiting_time.AVWaitingTimeCalculatorModule;
import ch.ethz.matsim.projects.astra.av.waiting_time.kernel.KernelWaitingTimeCalculatorConfigGroup;
import ch.ethz.matsim.projects.astra.av.waiting_time.zonal.ZonalWaitingTimeCalculatorConfigGroup;
import ch.ethz.matsim.projects.astra.config.ASTRAConfigGroup;
import ch.ethz.matsim.projects.astra.mode_choice.ASTRAModeChoiceModule;
import ch.ethz.matsim.projects.astra.scoring.ASTRAScoringModule;
import ch.ethz.matsim.projects.astra.traffic.ASTRATrafficModule;

public class RunScenario {
	static public void main(String[] args) {
		Config config = ConfigUtils.loadConfig(args[0], new ASTRAConfigGroup(), new AVConfigGroup(),
				new DvrpConfigGroup(), new AVPriceCalculationConfigGroup(), new AVWaitingTimeCalculatorConfigGroup(),
				new KernelWaitingTimeCalculatorConfigGroup(), new ZonalWaitingTimeCalculatorConfigGroup());

		config.global().setNumberOfThreads(Integer.parseInt(args[1]));
		config.qsim().setNumberOfThreads(Integer.parseInt(args[2]));

		Scenario scenario = ScenarioUtils.createScenario(config);
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(AVRoute.class, new AVRouteFactory());
		scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(DefaultEnrichedTransitRoute.class,
				new DefaultEnrichedTransitRouteFactory());
		ScenarioUtils.loadScenario(scenario);

		Controler controler = new Controler(scenario);

		controler.addOverridingModule(new DvrpTravelTimeModule());
		controler.addOverridingModule(new AVModule());
		controler.addOverridingModule(new BaselineModule());
		controler.addOverridingModule(new BaselineTransitModule());
		controler.addOverridingModule(new ZurichModule());
		controler.addOverridingModule(new ASTRATrafficModule());
		controler.addOverridingModule(new ASTRAScoringModule());
		controler.addOverridingModule(new ASTRAModeChoiceModule());
		controler.addOverridingModule(new ASTRAAVModule());
		controler.addOverridingModule(new AnalysisModule());
		controler.addOverridingModule(new ASTRAQSimModule());
		controler.addOverridingModule(new AVWaitingTimeCalculatorModule());
		controler.addOverridingModule(new AVPriceCalculationModule());
		controler.addOverridingModule(new ModeShareListenerModule());

		controler.addOverridingModule(new AVIdleModule());

		controler.run();
	}
}
