package ch.ethz.matsim.students_fs18.appenzeller;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.OutputDirectoryHierarchy.OverwriteFileSetting;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.IterationStartsEvent;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;

public class RunAnalysis {
	static public void main(String[] args) {
		String networkPath = args[0];
		String eventsPath = args[1];
		String outputPath = args[2];

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(networkPath);

		OutputDirectoryHierarchy outputHierarchy = new OutputDirectoryHierarchy(outputPath,
				OverwriteFileSetting.deleteDirectoryIfExists);
		outputHierarchy.createIterationDirectory(0);

		AVIdleListener listener = new AVIdleListener(network, outputHierarchy);

		EventsManager eventsManager = EventsUtils.createEventsManager();
		eventsManager.addHandler(listener);

		listener.notifyIterationStarts(new IterationStartsEvent(null, 0));
		new MatsimEventsReader(eventsManager).readFile(eventsPath);
		listener.notifyIterationEnds(new IterationEndsEvent(null, 0));
	}
}
