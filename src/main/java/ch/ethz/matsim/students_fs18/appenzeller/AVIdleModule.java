package ch.ethz.matsim.students_fs18.appenzeller;

import org.matsim.core.controler.AbstractModule;

public class AVIdleModule extends AbstractModule {
	@Override
	public void install() {
		addEventHandlerBinding().to(AVIdleListener.class);
		addControlerListenerBinding().to(AVIdleListener.class);
	}
}
