package ch.ethz.matsim.students_fs18.appenzeller;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.ActivityEndEvent;
import org.matsim.api.core.v01.events.ActivityStartEvent;
import org.matsim.api.core.v01.events.handler.ActivityEndEventHandler;
import org.matsim.api.core.v01.events.handler.ActivityStartEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.IterationStartsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.controler.listener.IterationStartsListener;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class AVIdleListener
		implements ActivityStartEventHandler, ActivityEndEventHandler, IterationStartsListener, IterationEndsListener {
	final private Set<Id<Person>> initialCache = new HashSet<>();
	final private Map<Id<Person>, IdleItem> activeItems = new HashMap<>();

	final private Network network;
	final private OutputDirectoryHierarchy outputHierarchy;

	@Inject
	public AVIdleListener(Network network, OutputDirectoryHierarchy outputHierarchy) {
		this.network = network;
		this.outputHierarchy = outputHierarchy;
	}

	@Override
	public void handleEvent(ActivityStartEvent event) {
		if (event.getPersonId().toString().startsWith("av_") && event.getActType().equals("AVStay")) {
			IdleItem item = new IdleItem();
			item.vehicleId = event.getPersonId();
			item.linkId = event.getLinkId();
			item.isInitial = initialCache.contains(event.getPersonId());
			item.startTime = event.getTime();
			item.coord = network.getLinks().get(event.getLinkId()).getCoord();

			activeItems.put(event.getPersonId(), item);
			initialCache.add(event.getPersonId());
		}
	}

	@Override
	public void handleEvent(ActivityEndEvent event) {
		if (event.getPersonId().toString().startsWith("av_") && event.getActType().equals("AVStay")) {
			IdleItem item = activeItems.remove(event.getPersonId());

			if (item != null) {
				item.endTime = event.getTime();
				writeItem(item);
			}
		}
	}

	@Override
	public void reset(int iteration) {
		initialCache.clear();
		activeItems.clear();
	}

	private class IdleItem {
		public Id<Person> vehicleId;
		public boolean isInitial;

		public Id<Link> linkId;
		public Coord coord;

		public double startTime;
		public double endTime;
	}

	private BufferedWriter writer;

	private void writeItem(IdleItem item) {
		try {
			writer.write(String.join(";",
					new String[] { item.vehicleId.toString(), item.linkId.toString(), String.valueOf(item.isInitial),
							String.valueOf(item.coord.getX()), String.valueOf(item.coord.getY()),
							String.valueOf(item.startTime), String.valueOf(item.endTime) })
					+ "\n");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void notifyIterationStarts(IterationStartsEvent event) {
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(outputHierarchy.getIterationFilename(event.getIteration(), "av_idle.csv"))));

			writer.write(String.join(";",
					new String[] { "vehicle_id", "link_id", "initial", "x", "y", "start_time", "end_time" }) + "\n");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		try {
			writer.flush();
			writer.close();
			writer = null;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
